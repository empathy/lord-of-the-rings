FROM node:9.6.1
RUN apt-get update
RUN apt-get upgrade -y
RUN npm install -g create-react-app
RUN npm install -g truffle
RUN npm install -g yarn

RUN apt-get install -y libgmp-dev

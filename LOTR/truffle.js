const HDWalletProvider = require("truffle-hdwallet-provider")
let provider

function getNmemonic() {
  try{
    return require('fs').readFileSync("./seed", "utf8").trim()
  } catch(err){
    return "";
  }
}

function getProvider(rpcUrl) {
  if (!provider) {
    provider = new HDWalletProvider(getNmemonic(), rpcUrl)
  }
  return provider
}

module.exports = {
  networks: {
    development: {
      host: "172.17.0.2",
      port: 8545,
      network_id: "*"
    },
    rinkeby: {
      get provider() {
        return getProvider("https://rinkeby.infura.io/")
      },
      network_id: 4,
    }
  }
}

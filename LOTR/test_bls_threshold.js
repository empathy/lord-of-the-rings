#!/usr/bin/env node
const bls = require('bls-lib')

/**
 * Verifies a contribution share
 * @param {Object} bls - an instance of [bls-lib](https://github.com/wanderer/bls-lib)
 * @param {Number} id - a pointer to the id of the member verifiing the contribution
 * @param {Number} contribution - a pointer to the secret key contribution
 * @param {Array<Number>} verificationVector - an array of pointers to public keys which is
 * the verification vector of the sender of the contribution
 * @returns {Boolean}
 */
function verifyContributionShare(bls, id, contribution, verificationVector) {
  const pk1 = bls.publicKey()
  bls.publicKeyShare(pk1, verificationVector, id)

  const pk2 = bls.publicKey()
  bls.getPublicKey(pk2, contribution)

  const isEqual = bls.publicKeyIsEqual(pk1, pk2)

  bls.free(pk1)
  bls.free(pk2)

  return Boolean(isEqual)
}

/////////////////////////////////////////////// <---------------- main


bls.onModuleInit(() => {
  // We are going to walk through Distributed Key Generation.
  // In DKG a group of members generate a "shared secret key" that none of them
  // know individal and public key. When a threshold amount of group members agree to sign on
  // the same message then then anyone can combine the signatures into a single
  // signature that can be verified against the groups public key
  //
  // The result of dkg be 1) each member will generate a secret key used to
  // sign messages for the group. 2) a group verification vector which contains
  // the groups public key as well as the the information need to derive any
  // of the members public key.
  //
  // Overview
  // 1) Each member will "setup" and generate a verification vector and secret
  // key contrubution share for every other member
  // 2) Each member post their verifcation vector publicly
  // 3) Each member sends their key contrubution share each other member
  // 4) When a member recieves a contrubution share it validates it against
  // the senders verifcation vector and saves it
  // 5) After members receive all thier contribution shares they compute
  // their secret key for the group

  bls.init()

  // to setup a group first we need a set a threshold. The threshold is the
  // number of group participants need to create a valid siganture for the group
  const threshold = 4
  // each member in the group needs a unique ID. What the id is doesn't matter
  // but it does need to be imported into bls-lib as a secret key
  const members = [10314, 30911, 25411, 8608, 31524, 15441, 23399].map(id => {
    const sk = bls.secretKey()
    bls.hashToSecretKey(sk, Buffer.from([id]))
    return {
      id: sk,
      recievedShares: []
    }
  })

  var ids = members.map(m => m.id) // an array of pointers containing the ids of the members of the groups

  console.log('Beginning the secret instantiation round...')
  
  const vvecs = [] // this stores an array of verifcation vectors. One for each Member

  // each member need to first create a verification vector and a secret key
  // contribution for every other member in the group (including itself!)
  members.forEach(id => {

    ////////////////////////////////////////////
    // generate a member contribution to the DKG
    ////////////////////////////////////////////

    var verificationVector = [] // this id's verification vector (is an array of public key pointers)
    var secretKeyContribution = [] // this id's sk contributions shares (is an array of secret key pointers)
    const svec = [] // this id's secret keys

    // generate a sk and verificationVector
    for (let i = 0; i < threshold; i++) {
      const sk = bls.secretKey()
      bls.secretKeySetByCSPRNG(sk)
      svec.push(sk)
  
      const pk = bls.publicKey()
      bls.getPublicKey(pk, sk)
      verificationVector.push(pk)
    }
  
    // generate key shares
    for (const id of ids) {
      const sk = bls.secretKey()
      bls.secretKeyShare(sk, svec, id)
      secretKeyContribution.push(sk)
    }
  
    svec.forEach(s => bls.free(s))
  

    // the verification vector should be posted publically so that everyone in the group can see it
    vvecs.push(verificationVector)

    // Each secret sk contribution is then encrypted and sent to the member it is for.
    secretKeyContribution.forEach((sk, i) => {
      // when a group member receives its share, it verifies it against the
      // verification vector of the sender and then saves it
      const member = members[i]
      const verified = verifyContributionShare(bls, member.id, sk, verificationVector)
      if (!verified) {
        throw new Error('invalid share!')
      }
      member.recievedShares.push(sk)
    })
  })

  // now each members adds together all received secret key contributions shares to get a
  // single secretkey share for the group used for signing message for the group
  members.forEach((member, i) => {

    const first = member.recievedShares.pop()
    member.recievedShares.forEach(sk => {
      bls.secretKeyAdd(first, sk)
      bls.free(sk)
    })
    member.secretKeyShare = first

  })
  console.log('-> secret shares have been generated')

  // Now any one can add together the all verification vectors posted by the
  // members of the group to get a single verification vector of for the group
  const groupsVvec = []
  vvecs.forEach(vvec => {
    vvec.forEach((pk2, i) => {
      let pk1 = groupsVvec[i]
      if (!pk1) {
        groupsVvec[i] = pk2
      } else {
        bls.publicKeyAdd(pk1, pk2)
        bls.free(pk2)
      }
    })
  })
  console.log('-> verification vector computed')

  // the groups verifcation vector contains the groups public key. The group's
  // public key is the first element in the array
  const groupsPublicKey = groupsVvec[0]

  const pubArray = bls.publicKeyExport(groupsPublicKey)
  console.log('-> group public key : ', Buffer.from(pubArray).toString('hex'))

  console.log('-> testing signature')
  // now we can select any 4 members to sign on a message
  const message = 'hello world'
  const sigs = []
  const signersIds = []
  for (let i = 0; i < threshold; i++) {
    const sig = bls.signature()
    bls.sign(sig, members[i].secretKeyShare, message)
    sigs.push(sig)
    signersIds.push(members[i].id)
  }

  // then anyone can combine the signatures to get the groups signature
  // the resulting signature will also be the same no matter which members signed
  const groupsSig = bls.signature()
  bls.signatureRecover(groupsSig, sigs, signersIds)

  const sigArray = bls.signatureExport(groupsSig)
  const sigBuf = Buffer.from(sigArray)
  console.log('->    sigtest result : ', sigBuf.toString('hex'))

  var verified = bls.verify(groupsSig, groupsPublicKey, message)
  console.log('->    verified ?', Boolean(verified))

  // clean up //
  bls.free(groupsSig)
  bls.freeArray(groupsVvec)
  members.forEach(m => {
    bls.free(m.secretKeyShare)
    bls.free(m.id)
  })
})

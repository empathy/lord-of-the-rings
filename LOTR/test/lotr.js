var Lotr = artifacts.require("Lotr");

async function assertRevert (promise, explanation) {
  try {
    await promise;
  } catch (error) {
    assert.include(error.message, "revert", explanation+" - transaction failed for some reason, but wasn't reverted");
    return;
  }
  assert(false, explanation + " - transaction succeeded, but should have failed");
}

contract('Lord of the Rings (accepted)', async (accounts) => {
  it("should be deployed successfully", async () => {
    let instance = await Lotr.deployed();
  });

  it("should correctly configure group size", async () => {
    let instance = await Lotr.deployed();

    let numMembers = await instance.numMembers();
    assert.equal(numMembers, 4, "incorrect number of group members");

    let threshold = await instance.threshold();
    assert.equal(threshold, 2, "incorrect signature threshold");
  });


  it("should correctly configure polynomial evaluation points", async () => {
    let instance = await Lotr.deployed();

    let polynomialEvalPoint0 = await instance.polynomialEvalPoints(0);
    assert.equal(polynomialEvalPoint0, 0, "incorrect polynomial evaluation point 0");

    let polynomialEvalPoint1 = await instance.polynomialEvalPoints(1);
    assert.equal(polynomialEvalPoint1, 7, "incorrect polynomial evaluation point 1");

    let polynomialEvalPoint2 = await instance.polynomialEvalPoints(2);
    assert.equal(polynomialEvalPoint2, 14, "incorrect polynomial evaluation point 2");

    let polynomialEvalPoint3 = await instance.polynomialEvalPoints(3);
    assert.equal(polynomialEvalPoint3, 21, "incorrect polynomial evaluation point 3");
  });

	it("should not let the scheme be finalised prematurely", async () => {
		let instance = await Lotr.deployed();

		await assertRevert(instance.finalizeScheme());
	});

  it("should correctly accept new members", async () => {
    let instance = await Lotr.deployed();

    assertRevert(
      instance.joinGroupSignature([1]),
      "too short polynomial",
    );

    assertRevert(
      instance.joinGroupSignature([1, 2, 3]),
      "too long polynomial",
    );

    // Add zeroth member
    let zerothMemberTxn = await instance.joinGroupSignature(
      [1, 2],
      {"from": accounts[0]},
    );

    let zerothMemberID = await instance.getMemberID(
      {"from": accounts[0]},
    )
    assert.equal(zerothMemberID, 0, "zeroth member ID incorrect");

    // Add first member
    let firstMemberTxn = await instance.joinGroupSignature(
      [3, 4],
      {"from": accounts[1]},
    );

    let firstMemberID = await instance.getMemberID(
      {"from": accounts[1]},
    );
    assert.equal(firstMemberID, 1, "first member ID incorrect");

    // Attempt checking member ID before joining the group
    await assertRevert(
      instance.getMemberID({"from": accounts[2]}),
      "checked member ID for an account that's not a member",
    )

    // Add second member
    let secondMemberTxn = await instance.joinGroupSignature(
      [5, 6],
      {"from": accounts[2]},
    );

    // Attempt approving secret points prematurely
    await assertRevert(
      instance.approveSecretPoints({"from": accounts[2]}),
      "premature approval of secret points - not all were shared",
    )

    // Add third member
    let thirdMemberTxn = await instance.joinGroupSignature(
      [7, 8],
      {"from": accounts[3]},
    );

    // Attempt adding too many members
    await assertRevert(
      instance.joinGroupSignature(
         [9, 10],
        {"from": accounts[4]},
      ),
      "attempting to add extra member",
    );
  });

  it("should correctly accept shared secret points", async () => {
    let instance = await Lotr.deployed();

    // Share secret points
    await instance.shareSecretPoints("a|b|c|d", {"from": accounts[0]});
    await instance.shareSecretPoints("a|b|c|d", {"from": accounts[1]});
    await instance.shareSecretPoints("a|b|c|d", {"from": accounts[2]});
    await instance.shareSecretPoints("a|b|c|d", {"from": accounts[3]});
  });

  it("should correctly accept protests and accepts", async () => {
    let instance = await Lotr.deployed();

    // Attempt protesting oneself
    await assertRevert(
      instance.protestSecretPoints(
        0x12,
        3,
        {"from": accounts[3]},
      ),
      "attempting to protest oneself",
    );

    // Approve secret points (starting from the end, to test issues
    // with initialisation of the array with responses)
    await instance.approveSecretPoints({"from": accounts[3]});
    await instance.approveSecretPoints({"from": accounts[2]});

    // Attempt protesting after having accepted
    await assertRevert(
      instance.protestSecretPoints(
        1337,
        2,
        {"from": accounts[3]},
      ),
			"a member cannot protest secret points after having initially accepted them",
    );

    // Finish approving secret points
    await instance.approveSecretPoints({"from": accounts[1]});
    await instance.approveSecretPoints({"from": accounts[0]});

    // Finalize the scheme
    await instance.finalizeScheme();

    schemeApproved = await instance.schemeApproved();
    assert.equal(schemeApproved, true, "scheme should be marked as approved");
  });

})

contract("Lord of the Rings (rejected)", async (accounts) => {
  it("should correctly accept new members", async () => {
    let instance = await Lotr.deployed();

    // Add members
    await instance.joinGroupSignature(
      [1, 2],
      {"from": accounts[0]},
    );

    await instance.joinGroupSignature(
      [3, 4],
      {"from": accounts[1]},
    );

    await instance.joinGroupSignature(
      [5, 6],
      {"from": accounts[2]},
    );

    await instance.joinGroupSignature(
      [7, 8],
      {"from": accounts[3]},
    );
  });

  it("should correctly accept shared secret points", async () => {
    let instance = await Lotr.deployed();

    // Share secret points
    await instance.shareSecretPoints("a|b|c|d", {"from": accounts[0]});
    await instance.shareSecretPoints("a|b|c|d", {"from": accounts[1]});
    await instance.shareSecretPoints("a|b|c|d", {"from": accounts[2]});
    await instance.shareSecretPoints("a|b|c|d", {"from": accounts[3]});
  });

  it("should not let members accept after rejecting", async () => {
    let instance = await Lotr.deployed();

    await instance.protestSecretPoints(
      1337,
      0,
      {"from": accounts[1]},
    );

    memberProtested = await instance.memberProtested();
    assert.equal(memberProtested[0], 1, "wrong ID of protesting member");
    assert.equal(memberProtested[1], 1337, "wrong secret point");
    assert.equal(memberProtested[2], 0, "wrong ID of protested member");
    assert.equal(memberProtested[3], true, "incorrect protest status");

    await instance.approveSecretPoints({"from": accounts[0]});
    member0Approved = await instance.memberApproved(0);
    assert.equal(member0Approved, true, "member 0 not marked as approved");
  });
})
